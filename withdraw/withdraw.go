package withdraw

import (
	"atm-solutions/storage"
	"atm-solutions/streaming"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

type WithdrawRequest struct {
	Amount int `json:"amount"`
}

type WithdrawResponse struct {
	Banknote1000   int
	Banknote500    int
	Banknote100    int
	WithdrawAmount int
	Balance        int
}

func calculateBanknote(amount int, banknote int) (int, int) {
	if amount < banknote {
		return 0, amount
	}

	count := amount / banknote
	remaining := amount - (banknote * count)
	return count, remaining
}

func withdrawMoney(amount int) WithdrawResponse {
	storage.MyMoney = storage.MyMoney - amount

	bn1000, remaining := calculateBanknote(amount, 1000)
	bn500, remaining := calculateBanknote(remaining, 500)
	bn100, remaining := calculateBanknote(remaining, 100)

	return WithdrawResponse{
		Banknote1000:   bn1000,
		Banknote500:    bn500,
		Banknote100:    bn100,
		WithdrawAmount: amount,
		Balance:        storage.MyMoney,
	}
}

func HandleWithdraw(w http.ResponseWriter, r *http.Request) {
	w.Header().Add("Content-Type", "application/json")
	b, _ := ioutil.ReadAll(r.Body)
	wr := WithdrawRequest{}
	json.Unmarshal(b, &wr)

	if wr.Amount < 1 || wr.Amount%100 != 0 || wr.Amount > storage.MyMoney {
		json.NewEncoder(w).Encode("การร้องขอล้มเหลว")
	} else {
		streaming.SendAsync(fmt.Sprintf("Gopher ถอนเงิน %v บาท", wr.Amount))
		json.NewEncoder(w).Encode(withdrawMoney(wr.Amount))
	}
}
