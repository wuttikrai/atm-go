package main

import (
	"atm-solutions/balance"
	"atm-solutions/deposit"
	"atm-solutions/withdraw"
	"log"
	"net/http"

	"github.com/gorilla/mux"
)

func main() {
	router := mux.NewRouter()
	router.HandleFunc("/balance", balance.HandleBalance).Methods("GET")
	router.HandleFunc("/deposit", deposit.HandleDeposit).Methods("POST")
	router.HandleFunc("/withdraw", withdraw.HandleWithdraw).Methods("POST")
	log.Fatal(http.ListenAndServe(":9000", router))
}
