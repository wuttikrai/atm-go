package balance

import (
	"atm-solutions/storage"
	"atm-solutions/streaming"
	"encoding/json"
	"fmt"
	"net/http"
)

type GetBalanceResponse struct {
	Balance int
}

func getBalance() GetBalanceResponse {
	return GetBalanceResponse{
		Balance: storage.MyMoney,
	}
}

func HandleBalance(w http.ResponseWriter, r *http.Request) {
	w.Header().Add("Content-Type", "application/json")
	balance := getBalance()
	streaming.SendAsync(fmt.Sprintf("Gopher มียอดเงินคงเหลือ %v บาท", balance.Balance))
	json.NewEncoder(w).Encode(balance)
}
