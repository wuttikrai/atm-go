package streaming

import "bitbucket.org/dotography-code/luciano/kafka/producer"

func SendAsync(message string) {
	producer := producer.KafkaProducer{
		Topic:      "golang",
		MaxRetry:   5,
		BrokerList: []string{"192.168.212.212:9092"},
	}
	producer.Send("workshops", message)
}
