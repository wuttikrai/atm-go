package deposit

import (
	"atm-solutions/storage"
	"atm-solutions/streaming"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

type DepositRequest struct {
	Amount int `json:"amount"`
}

type DepositResponse struct {
	DepositAmount int
	Balance       int
}

func depositMoney(amount int) DepositResponse {
	storage.MyMoney = storage.MyMoney + amount

	return DepositResponse{
		DepositAmount: amount,
		Balance:       storage.MyMoney,
	}
}

func HandleDeposit(w http.ResponseWriter, r *http.Request) {
	w.Header().Add("Content-Type", "application/json")
	b, _ := ioutil.ReadAll(r.Body)
	dr := DepositRequest{}
	json.Unmarshal(b, &dr)

	if dr.Amount < 1 || dr.Amount%100 != 0 {
		json.NewEncoder(w).Encode("การร้องขอล้มเหลว")
	} else {
		streaming.SendAsync(fmt.Sprintf("Gopher ฝากเงิน %v บาท", dr.Amount))
		json.NewEncoder(w).Encode(depositMoney(dr.Amount))
	}
}
